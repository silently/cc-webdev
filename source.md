class: center, middle

# Web Development
# Crash Course

.author[Guillaume Denis - 2019]

.cc[![CC](assets/img/cc.png)]
.cc[![CC](assets/img/by.png)]
.cc[![CC](assets/img/sa.png)]

.author.small[Slides [source](https://gitlab.com/silently/cc-webdev), created using [remark](https://github.com/gnab/remark)]
---
name: agenda

# Agenda

.internal[[Aside: Working with Git](#git)]

.internal[[Front-End Introduction](#front)]

.internal[[HTTP & Server Stack](#HTTP)]

.internal[[Back-End Development with Node.js](#node)]

.internal[[Front-End: SPA & Tools](#SPA)]

---
name: git
class: center, middle

# Aside: Working with Git

---

# What is Git?

* Git is a distributed version-control system for tracking changes in source code during software development
* Why a VCS?
    * level 0: back-up your codebase and its history
    * level 1: foster teamwork
    * level 2: manage projects (shared workflows for features and issues, releases, versions...)
* Why Git?
    * Distributed (local & remote repositories)
    * Feature branches
    * Hosted services: **GitLab**, GitHub, Bitbucket (pull requests, issues, boards, CI..)
* [Concepts](https://dev.to/unseenwizzard/learn-git-concepts-not-commands-4gjc)
    * Development environment: working directory, staging area, local repository
    * Branching, merging, conflicts (don't be afraid)
* Resources: [simple guide](http://rogerdudler.github.io/git-guide/), [cheat sheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet/) by GitHub

---

# Tutorial: Git commands

Guided tutorial:
```shell
mkdir project && cd project
git init
# check .git, create readme, .gitignore
git add README.md
git add .gitignore
git commit -m "Repo setup and readme"
git log --pretty=oneline
# New modifications
git diff
```

More:
* What is the difference between `rm` and `git rm`?
* How to undo things?
---

# Feature Branch Workflow

* Create a new branch (and focus on its related feature )
```shell
git checkout -b new-feature
```
* Commit changes
```shell
git status
git add <some-file>
git commit
```
* Push branch
```shell
git push -u origin new-feature
```
* Create a pull request (on website interface)
* Process review (new commits and possibly rebase)
* Merge to master
```shell
git checkout master
git pull
git pull origin new-feature
git push
```
* More suggestions on [GitLab Workflow](https://docs.gitlab.com/ee/workflow/gitlab_flow.html)

---
# Tutorial: Gitlab Setup (1/2)

* Create a `<nickname>` account on gitlab.com
* Create a `<nickname>-groups` and a `tutorials` subgroup
* Create a new private repository (named `form`) within `tutorials`
* Publish a first commit:
```shell
mkdir form && cd form
git init
git remote add origin git@gitlab.com:<nickname>/tutorials/form.git
touch README.md
git add README.md
git commit -m "Initial commit"
git push -u origin master
```
* Check `.git/config` contents
* Create a new issue label (named `feature`)
* Create a new issue (named `Create homepage` with the `feature` label)

---

# Tutorial: Gitlab Workflow (1/2)

* Create the feature branch and implement the feature
```shell
git checkout -b homepage
touch index.html
git add index.html
git commit -m "Homepage creation"
# edit index.html
git add index.html
git commit -m "New homepage contents"
git push -u origin homepage
```
* See new branch on gitlab repository
* Create a merge request
    * With "Implements #1" description…
    * "Delete source branch…" and "Squash commits" options
* Merge!
* Check Repository, Issues and Merge requests sections

---
name: front
class: center, middle

# Front-End Introduction

---

# HTTP Clients

* HTTP clients examples:
    * Desktop and mobile browsers
    * Apps (webviews or custom clients)
    * Servers (calling external services)
    * Command line tools ([curl](https://devhints.io/curl) or [httpie](https://httpie.org/)) and libraries ([libcurl](https://curl.haxx.se/libcurl/))
    * SEO bots
* Browser features:
    * Networking: HTTP client + security + maintaining client state (eg cookies)…
    * UX: browser interface (eg address bar, back button) + web page interaction
    * Rendering engine
    * JavaScript engine
    * Data storage
    * Multiple tabs
    * Developer tools!

---

# From HTML to the DOM

* Web page loading in a browser:
    * Download initial page (`index.html` for instance) and parse it
    * Download additional linked resources (CSS, JS, images) and parse them
    * Wait for scripts to be parsed and interpreted (or use `async` not to block DOM construction)
    * Build the DOM (+ CSSOM and the render tree)
    * Render the web page and emit `DOMContentLoaded`)
* The DOM is an interface allowing scripts to interact with the webpage (live object-oriented version of the HTML):
    * add/modify/remove HTML elements
    * change attributes and style
    * listen and react to events
* .tutorial[Tutorial]: inspect [Hacker News](https://news.ycombinator.com) web page with the browser developer tools
    * Elements tab: interact with the DOM
    * Console tab: see what APIs are available and interact with the DOM
    * Network tab: visualize the page loading process

---

# Role of HTML

* HTML is a markup language used for different purposes:
    * Define contents served to the user (text, image, video, audio, 3D…)
    * Define contents served to indexing bots (SEO)
    * Define basic interactions (links, forms…)
    * Organize contents for further styling (with CSS)
    * Organize contents for further interaction (with JS)
* Alternatives
    * HTML may be generated server-side (and modified client-side)
    * …and not written by hand,
    * but in the end web pages are loaded and parsed as HTML

---

# HTML Syntax

XML-based syntax:

* define elements with opening/closing tags and inner contents:
```html
<nav>
      <ul>
        <li>Home</li>
        <li>Products</li>
        <li>Contact</li>
      </ul>
</nav>
```
* add attributes:
```html
<p class="highlight">paragraph</p>
<a href="/contact">Contact</p>
```
* some tags are self-closing:
```html
<img src="/assets/img/logo.png" alt="Logo"/>
<link rel="stylesheet" href="/css/main.css" />
```

---

# HTML Tags and Structure

Typical tags (non-exhaustive, [see documentation](https://developer.mozilla.org/en-US/docs/Web/HTML/Element)):
* Main root `html` divided into `head` and `body`
```html
<!DOCTYPE html>
<html lang="en">
      <head>
        <meta charset="utf-8">
        <title>Page Title - Website Name</title>
        <link rel="stylesheet" href="/css/main.css" />
        <script src="/js/main.js"/></script>
      </head>
      <body>
        <!-- contents -->
      </body>
</html>
```
* Content sectioning: `header`, `footer`, `nav`, `main`, `section`, `h1`, `h2`…
* Text contents: `div`, `p`, `ul`, `ol`, `li`, `table`
* Inline text semantics: `span`, `a`, `em`, `small`, `code`
* Multimedia contents: `img`, `video`, `audio`, `svg`, `canvas`
* Forms: `form`, `input`, `label`, `button`, `textarea`

---

# HTML Attributes

Typical attributes (non-exhaustive, [see documentation](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes))
* Global attributes: `id`, `class`, `tabindex`, `title`
* `name` and `content` for: `<meta>`
* `src` for: `<script>`, `<img>`, `<video>`, `<audio>`
* `href` for: `<link>`, `<a>`
* `type` for: `<script>`, `<input>`, `<button>`, `<style>`
* specific attributes to configure forms
```html
<form action="/subscribe" method="get" class="inline-form">
      <div class="field">
        <label for="email">Enter your email: </label>
        <input type="email" name="email" id="email" required>
      </div>
      <div class="field">
        <input type="submit" value="Subscribe!">
      </div>
</form>
```
* most of the time avoid styling attributes (and prefer CSS): `style`, `align`, `background`, `border`… (counter example: `width` and `height` for `<canvas>`)

---

# HTML Tutorial: Forms

* Clone the `https://gitlab.com/silently/postback` repository and launch the app (see the readme)
* Create a different project (and folder) containing a registration form in `index.html`
* The registration form should:
    * Post to `http://localhost:3000` (to check the result of the post)
    * Collect nickname, email, password and agreement (checkbox)
    * Use: labels, input types, submit button
    * Extra: nickname should contain at least 3 characters (only letters and numbers)
    * Extra: add a `<select>` form control

---
name: CSS

# Styling With CSS

CSS rule syntax:
![CSS](assets/img/css-selectors-lrg.png)
.attribution[[Source: wordpress.com](https://en.support.wordpress.com/custom-design/css-basics/)]

Link CSS files in HTML head:
```html
<html>
  <head>
    <link rel="stylesheet" href="css/main.css" />
  </head>
…
```
---

# Page Rendering & Normal Flow

* [Normal flow](https://www.w3.org/TR/CSS2/visuren.html#normal-flow) with *block* (full-width), *inline* and *none* display types
* Normal flow disruptive factors:
    * Children of an element with display type: table, [flex](https://css-tricks.com/snippets/css/a-guide-to-flexbox/), [grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
    * Position property (consider mainly for page wise fixed elements and for multi-layers)
    * Float property (consider mainly for elements surrounded by text)
    * Specifying sizes
* [Box model](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Box_model) and `box-sizing`

---

# CSS Selectors

Non-exhaustive summary, [see documentation](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors):

* Simple selectors:
    * tag type: `p {…}`
    * id (one per element): `#email {…}`
    * class (possibly several per element): `.selected {…}`
    * attribute (may test if: exists, matches, contains, starts with…): `[type="text"] {…}`
    * universal: `* {…}`
* Combinators
    * two different selectors: `p, ul`
    * descendant: `main .selected {…}`
    * direct child: `main > .selected {…}`
    * adjacent sibling: `h2 + p` (will match all `<p>` that directly follow an `<h2>`)
* Pseudo-classes (elements with a special state): `:hover {…}` and `:focus`, `:empty`, `:first-child`, `:nth-child()`, `:not()`…
* Pseudo-elements (targets elements subparts): `::before {…}` and `::after`, `::first-letter`…
* Combination example: `nav ul.main li a:hover`

---

# CSS Principles

* Cascading:
  1. ~~Importance~~
  2. Specificity (see [article](https://css-tricks.com/specifics-on-css-specificity/) and Developer Tools Styles section)
  3. Source order (overriding)
* Inheritance:
    * `body { color: blue; }` will define a default text color for any child node of body
    * Not applied to certain properties (eg margin)
    * Possibility to control: `p { color: revert; }`
* Media queries
```css
@media (min-width: 801px) {
      body {
        margin: 0 auto;
        width: 800px;
      }
}
```

Documentation: [cascading](https://developer.mozilla.org/en-US/docs/Learn/CSS/Introduction_to_CSS/Cascade_and_inheritance) and [media queries](https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries)

---

# CSS Properties and Values

* List of [common properties](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Properties_Reference)
* Values and units:
    * Size: `10px`, `2em`, `80%`, `0.5vw`, `0`
    * Duration: `0.3s`, `300ms`
    * Color: `white`, `#fff`, `#ffffff`, `rgba(255, 255, 255, 1)`
    * And many more: `font-weight: bold;`
* Shorthand notation:
    * Examples:
    ```css
    border: 1px solid red;
    background: url("back.jpg") no-repeat cover;
    padding: 20px 10px 20px 10px;
    padding: 10px;
    ```
    * Shorthand properties: `font`, `background`, `padding`, `border`, `margin`, `transition`
* Transitions (and animations)
  ```css
  nav a { transition: opacity 200ms linear; }
  nav a:hover { opacity: 0.8; }
  ```
---

# CSS Tools

* Preprocessors (Sass/SCSS, less, styles), SCSS example:

<table>
<tr>
<td>

```scss
# SCSS
$primary-color: #6495ED;

nav {
  li { display: inline-block; }

  a {
    color: $primary-color;
    text-decoration: none;
  }
}
```
</td>
<td>

```scss
# Generated CSS
nav li { display: inline-block; }
nav a {
    color: #6495ED;
    text-decoration: none;
}
```
</td>
</tr>
</table>
* Libraries: [Font Awesome](https://fontawesome.com/), [Normalize.css](https://necolas.github.io/normalize.css/), [Animate.css](https://daneden.github.io/animate.css/), [Hint.css](https://kushagra.dev/lab/hint/)
* Frameworks: [Bootstrap](https://getbootstrap.com/), [Foundation](https://foundation.zurb.com/), [Semantic UI](https://semantic-ui.com/), [Materialize](https://materializecss.com/)
* Bundlers (eg [webpack](https://webpack.js.org/)) for compilation, concatenation, minification…

---

# CSS Tutorial

* [Play with selectors](https://flukeout.github.io/)
* Style the previous form:
    * Highlight the focused input
    * With flexbox: stack inputs vertically, align left (except for submit button)
    * Transition when mouse is over submit button

---

# JS Overview

* Initially created for browsers, today JS engines may be used in different contexts (V8 -> Node.js)
* Follows the ECMAScript specification with enhancements ([ES6](https://devhints.io/es6), ES2016, ES2017…)
* Typical C-based syntax
* In browsers, JS code has:
    * access to `window`, `document`, `location`, `navigator` objects
    * access to specific APIs: Service Workers, Audio API, WebRTC…
    * security limitations (filesystem, communication with other tabs, cookies access, CORS…)
* With Node.js, JS code:
    * is not tied to a browser window
    * is executed as an application
* Alternative: possibility to program in languages that compile to JS (TypeScript, Flow, Dart…)
* Resources: [javascript.info](https://javascript.info/), [MDN Reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference), [ECMAScript 2019 & beyond](https://itnext.io/status-of-javascript-ecmascript-2019-beyond-5efca6a2d233)

---

# JS Declarations and Modules

Declare variables with `let` or `const`
```javascript
// let
let str = 'string'; // declaration + assignment
str = str.toUpperCase();
// const (unique assignment)
const obj = { name: "Kay"};
obj = { name: "Jay"}; // error
obj.name = "Jay"; // ok
// don't use these
var x = 1; // scoping problems with var
y = 2; // global variable

```
Export and import modules:
<table>
<tr>
<td>

```javascript
// timer.js module
const had = 24;
export default class Timer {…};
export function start() {…};
export const stop = function() {…};
export { had as HOURS_A_DAY };
```
</td>
<td>

```javascript
// timer.js "client"
import Timer from 'timer.js';
import { start } from 'timer.js';
import { stop, HOURS_A_DAY } from 'timer.js';
import { HOURS_A_DAY as HAD } from 'timer.js';
import * as TimerModule from 'timer.js';
import Timer, * as TimerModule from 'timer.js';
```
</td>
</tr>
</table>
[Dynamic imports](https://v8.dev/features/dynamic-import) are [coming](https://caniuse.com/#feat=es6-module-dynamic-import)

---

# JS Functions

```javascript
// function declaration
function sum(x, y) { return x + y; }
// assigned function expression
const sum = function(x, y) { return x + y; }
// default parameters
const sum = function(x, y = 0) { return x + y; };
// rest parameters (indefenite number of arguments put in array)
const sum = function(x, ...args) { … };
// destructuring (works with arrays too)
let data = { id: 1, … };
const getId = function({id}) { return id; }; // getId(data) returns 1

// arrow functions syntax (shorter, better scoping)
const sum = (x,y) => { return x + y };
const sum = (x,y) => x + y; // return not needed if only one expression
const encapsulate = (d) => ({ data: d }); // parenthesis needed to return an object
const identity = x => x; // parenthesis not needed if one argument
const noop = () => {}; // parenthesis needed if no argument
```

---

# JS Classes

```javascript
class Menu extends Component {
  static count = 0; // static field
  visible = false; // public instance field
  #mounted = false; // private instance field
  constructor(props) {
    super(props);
  }
  static staticMethod() {…}
  method() {…}
  boundMethod = () => {…}
  get area() {…}
  set area() {…}
}
```

More coming (decorators, mixins...) and some experimental features may needed a compiler (see [Babel](https://babeljs.io/docs/en/babel-plugin-proposal-class-properties)) for compatibility.
---

# Asynchronous JS: Overview

See [Callback Hell](http://callbackhell.com/)

```javascript
const queryArticle = 'SELECT author_id FROM articles WHERE title = "JS" LIMIT 1';
const queryAuthor = 'SELECT * FROM users WHERE id = $1 LIMIT 1';

// Using callbacks
client.query(queryArticle, (err, res) => {
    // err and res are only available in the callback function
    const id = res.rows[0].author_id;
    client.query(queryAuthor, id, (err, res) => {
        // do something with author
    }
});

// Using Promises for better chaining
client.query(queryArticle)
  .then(res => return client.query(queryAuthor, res.rows[0].author_id))
  .then(res => /* do something with author */)
  .catch(e => console.error(e.stack));

// Using async/await for easier syntax
try {
  let article = await client.query(queryArticle).rows[0];
  let author = await client.query(queryAuthor, article.author_id);
  // do something with author
} catch (err) {
  console.log(err.stack);
}
```

---

# Asynchronous JS: Promises

* Possibility to write by hand (`resolve` → `then`, `reject` → `catch`)
```javascript
function waitFor(delay) {
      return new Promise(function(resolve, reject) {
          if(delay < 0) reject();
          setTimeout(resolve, delay);
      });
}
function loadImage(url) {
      return new Promise(resolve, reject => {
        const image = new Image();
        image.onload = () => resolve(image);
        image.onerror = reject;
        image.src = url;
      });
}
loadImage('/assets/img/logo.png')
  .then(img => …)
  .catch(err => …);
```
* Some APIs and libraries create promises for us ([fetch](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch), [json stream reader](https://developer.mozilla.org/en-US/docs/Web/API/Body/json), database clients):
```javascript
fetch(request)
      .then(response => response.json())
      .then(data => …);
```
* `async` function prefix has the function returns a Promise
* [Methods](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise#Methods) to help manage several promises: `Promise.all`, `Promise.race`

---

# Asynchronous JS: async/await

* Relies on Promises
* `async` before a function:
    * Makes it return a promise
    * Allows to use `await`
* Possibility to mix with Promise API:
```javascript
async function fetchAll() {
      return await Promise.all([fetch('user1'), fetch('user2')]);
}
```
* Works for class methods
* Error handling needed (`try`/`catch`)

---

# JS Other Features

* Destructuring
```javascript
let kay = { firstname: "Kay" };
let { firstname } = kay; // "Kay"
let [head] = [1, 2, 3]; // 1
```
* Shorthand object creation
```javascript
let firstname = "Kay";
let kay = { firstname }; // { firstname: "Kay" }
```
* Spread operator
```javascript
const options = { ...defaults, visible: true };
const users = [ ...admins, { name: "Kay"} ];
```
* For…of iteration
```javascript
 for (let i of iterable) {
  ···
}
```
* String interpolation
```javascript
const firstname = "Kay";
let hello = `Hello ${firstname}`;
```

---

# JS Tutorial

* Consider the following data (France employment sectors in 2016):
    * Agriculture: 754,000
    * Industry: 3,626,300
    * Construction: 1,699,000
    * Services: 20,161,000
* Create a web page and display a vertical and coloured stacked histogram:
    * Dynamically load the data from this [JSON file](https://gitlab.com/silently/cc-webdev/raw/master/assets/data/employment.json) with JS
    * Display the `info` field as the page `<h1>`
    * Create stacked and coloured HTML elements with their height proportional to their employment rate

---
name: HTTP
class: center, middle

# HTTP & Server Stack

---

# Web Protocols

* The Web: interconnected network/s of electronic devices
* Resources served by devices are identified by URLs, following this [generic syntax](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier#Generic_syntax) (also used by other protocols, see `postgres` below):
  ```shell
  *scheme://hostname:port/path
  http://localhost:3000/path
  https://www.w3.org/TR/url/
  postgres://<user>:<pwd>@localhost:5432/<databasename>
  ```
* DNS (Domain Name System) is a decentralized system that associates domain names and IP addresses
* TCP/IP (internet and transport layer) protocol suite enables communication over the network:
    * IP deals with addressing and routing
    * TCP establishes and maintains connections
* HTTP (application layer) is an assymetric client-server protocol with the following properties:
    * typical request/response protocol
    * stateless (servers don't hold any information on previous requests, at the protocol layer)
    * enables caching capabilities
    * connections may be upgraded (keep-alive, web sockets)
    * secure communication with HTTP over TLS (HTTPS)
* More in these [NTU course notes](https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_Basics.html)

---
class: center, middle

![DNS](assets/img/dns.jpg)
.attribution[[Source: DNS security presentation](https://www.slideshare.net/srikrupa5/dns-security-presentation-issa)]

---

# HTTP Request

![DNS](assets/img/HTTP-request.png)
.attribution[[Source: Chua Hock-Chuan](https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_Basics.html)]

* HTTP [request methods](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#Request_methods): GET, HEAD, POST, PUT, PATCH, DELETE…
* Specific headers for:
    * Sending cookies
    * HTTP authentication
    * Caching settings
    * CORS settings

---

# HTTP Response

![DNS](assets/img/HTTP-response.png)
.attribution[[Source: Chua Hock-Chuan](https://www.ntu.edu.sg/home/ehchua/programming/webprogramming/HTTP_Basics.html)]

* HTTP status codes:
    * `2xx` (success)
    * `3xx` (redirects)
    * `4xx` (client error)
    * `5xx` (server error)
* Specific headers for:
    * Setting cookies
    * Caching properties (Last-Modified, ETag, Expires…)

---

# HTTPS

* Secured communication over an insecure network
* Through bidirectional encryption (TLS and previously SSL) + certificate authorities
* Not encrypted: destination (IP host and port)
* Protection (privacy and integrity) against man-in-the-middle attacks
* Managed by browser + HTTP server configuration
* SEO impact

# HTTP/2

* HTTP headers are split (frames), compressed (HPACK) and binary encoded
* A single TCP connection used for request & response**s** (with bidirectional streams, multiplexing, prioritization) per origin
* Server push
* Support (2019): implemented by main servers and [clients](https://caniuse.com/#search=http2) latest versions
* More [here](https://http2-explained.haxx.se/content/fr/)
* HTTP/3: UDP based [draft](https://quicwg.org/base-drafts/draft-ietf-quic-http.html) (and [more](https://http3-explained.haxx.se/en/))


---
class: bottom
background-image: url(assets/img/arch1.png)

.attribution[Server Architecture #1,<br/> [source: Donne Martin](https://github.com/donnemartin/system-design-primer)]

---
class: bottom
background-image: url(assets/img/arch2.png)

.attribution[Server Architecture #2,<br/> [source: Donne Martin](https://github.com/donnemartin/system-design-primer)]

---
class: bottom
background-image: url(assets/img/arch3.png)

.attribution[Server Architecture #3,<br/> [source: Donne Martin](https://github.com/donnemartin/system-design-primer)]

---

# Authentication and Authorization

* Application level typical setup:
    * Server sets a session ID in a cookie
    * Additional data (the *session*) is linked (server side) to this session ID
    * → **stateful**
* Protocol level:
    * Basic Auth (implies HTTPS for security), easy set-up
    * Bearer Token (OAuth 2.0):
        * Server shares a signed token whose integrity will be checked during further requests
        * Client decides how to store the token (eg local storage or cookie)
        * Token contains all the data needed for subsequent requests (claims)
        * → **stateless** (no DB hit from session ID + better scalability but JWT size is bigger)
        * Common implementation: [JSON Web Tokens](https://jwt.io/introduction/)
* More [here](https://dzone.com/articles/cookies-vs-tokens-the-definitive-guide)

---
name: node
class: center, middle

# Back-End Development
# with Node.js

---

# Node.js

* JavaScript runtime built on Chrome's V8 JavaScript engine
* Available on many [platforms](https://nodejs.org/en/download/)
* Non-blocking event loops (asynchronous code)
* Modules:
    * Core (http, fs) VS community (express, moment, eslint)
    * May be native (C++)
* Community-based modules rely on a manager ([npm](https://www.npmjs.com/) or [yarn](https://yarnpkg.com/fr/)) and a configuration file (package.json)
```JSON
{
      "name": "beep-starter",
      "version": "1.0.0",
      "repository": "gitlab:silently/beep-starter",
      "scripts": {
        "build": "npx babel src -d build",
        "dev": "nodemon --exec babel-node src/index.js",
        "lint": "npx eslint --ext .js src",
        "start": "node build/index.js",
        "test": "npx mocha --require @babel/register test/**/*.spec.js"
      },
      "dependencies": {
        "express": "^4.17.1",
        "pg": "^7.12.1"
      },
      "devDependencies": {
        "nodemon": "^1.19.1"
      }
}
```


---

# Express

* Minimalist web framework for Node.js
* Provide convenient features:
    * [Routing](http://expressjs.com/en/guide/routing.html) and HTTP handling with [req](http://expressjs.com/en/5x/api.html#req) and [res](http://expressjs.com/en/5x/api.html#res) JS objects
    * Built-in middleware: serve static files, parse JSON and urlencoded request
    * 3rd-party middleware: eg cookie parser, compression, security headers
    * Template engines
* .tutorial[First Express app tutorial]:
    * Follow the [install instructions](http://expressjs.com/en/starter/installing.html)
    * Create `index.js` in the project directory
    * Install nodemon: `npm install --save-dev nodemon`
    * Edit `package.json` with a `"start"` script: `nodemon app.js`
    * Launch (in the terminal): `npm start`
    * Create a `/now` route that displays the current time
    * Serve a static file (image)

---

# Express Typical Structure

![mvc](assets/img/mvc.png)
.attribution[[Source: MDN](https://developer.mozilla.org/en-US/docs/Learn/Server-side/Express_Nodejs/routes)]

---

# Testing with Mocha

* [Mocha](https://mochajs.org/) is test framework (write specs, run them serially, get a report)
* May be enhanced by [chai](https://www.chaijs.com/) assertion library and [plugins](https://www.chaijs.com/plugins/)
* [Discussion](https://codeutopia.net/blog/2015/04/11/what-are-unit-testing-integration-testing-and-functional-testing/) about TDD and test types (unit, integration, functional or E2E)

Integration test example:
```javascript
describe('Homepage', () => {
      describe('GET /', () => {
        it('should contain hello', async () => {
           const res = await chai.request(app).get('/');
           res.should.have.status(200);
           res.text.should.contain('hello');
         });
      });
});
```

---

# Receipts Manager: Warmup (1/3)

* Proposal for a Express/PostgreSQL project starter [beep-starter](https://gitlab.com/silently/beep-starter):
* Debatable defaults, not one fit-all solution, consider:
    * Layouts, models, translations, seeds folders... for project structure
    * Adding common middlewares (like [helmet](https://github.com/helmetjs/helmet), [compression](https://github.com/expressjs/compression))
    * See others [(1)](https://github.com/danielfsousa/express-rest-es2017-boilerplate) or [(2)](https://github.com/sahat/hackathon-starter)
* .tutorial[Tutorial warmup]:
    * Clone [beep-starter](https://gitlab.com/silently/beep-starter) and follow the readme
    * Install **linter-eslint** atom package
    * Create PostgreSQL user and database on your local environment
    * Run tests, run the app (go to path `/`), check the DB connection (go to path `/now`)

---

# PostgreSQL

* SQL-based RDBMS (others include: sqlite3, MySQL, Oracle)
* Characteristics:
    * Free and open source, managed by a community of developers
    * Compliance to SQL standards
    * Multiplatform
    * Robustness
* Highlights:
    * Full-text search
    * Partitioning, replication
    * Data Types: primitives, structured(arrays), documents(JSON/JSONB, key-value)
    * Aggregate functions
    * Parallel query support
    * Transactions, savepoints

---

# SQL Introduction

* Structured Query Language to manage data stored in tables (non-exhaustive):
    * Schema statements:
        * `CREATE`, `ALTER`, `DROP`
        * for `DATABASE`, `TABLE`, `INDEX`, `VIEW`
    * Data statements: `SELECT`, `INSERT INTO`, `UPDATE`, `DELETE FROM`
    * Control statements: `GRANT`, `REVOKE`
    * Transaction statements: `START TRANSACTION`, `SAVEPOINT`, `COMMIT`, `ROLLBACK`
    * and also `CREATE USER`, `CREATE ROLE`, `EXPLAIN`...
* Main concepts:
    * Tables, columns, rows
    * Queries, subqueries, result sets
    * Primary key, foreign key
    * Entity-relationships, normalization
* SQL is a declarative language (describe **what** we want instead of **how**)
* But we define **how** the data is structured (tables, columns, types, index, constraints...)
* Queries depend on schemas / schemas are (re)thought to serve queries

---

# SQL SELECT Example

```SQL
SELECT c.name, COUNT(*) num_unhabitants
FROM cities c INNER JOIN people p
ON c.id = p.city_id
WHERE p.status = 'active'
GROUP BY c.name
HAVING COUNT(*) > 1000
ORDER BY num_unhabitants DESC;
```

This is just a glimpse, here are some pointers:
* An [introduction](https://github.com/DarrellAucoin/IntroSQL/wiki) or a tutorial on [database design](https://www.ntu.edu.sg/home/ehchua/programming/sql/Relational_Database_Design.html)
* [Typical queries](https://github.com/enochtangg/quick-SQL-cheatsheet) and [cheatsheet](https://devhints.io/mysql)
* More [aggregate functions](https://www.postgresql.org/docs/11/functions-aggregate.html)
* SQL and pandas [comparison](https://pandas.pydata.org/pandas-docs/stable/getting_started/comparison/comparison_with_sql.html)

---

# Receipts Manager: Implementation (2/3)

* .tutorial[Tutorial features]:
    * Page to upload shopping receipts in CSV format:
        * CSV file name follows this format: `date-index-user.csv`
        * CSV headers are: `label,per_kilo,unit_cost,quantity,price`
    * Page(s) to browse insigths:
        * User: purchase frequency, average items per purchase, average total cost per receipt
        * Product: purchase frequency, total cost
    * Sample data available [here](https://gitlab.com/silently/cc-webdev/raw/master/assets/data/receipts.zip)
* Design and organize:
    * Meeting to split tasks, design database, list client-side URLs
    * Create project on GitLab, invite developers, create issues
* Implement:
    * Feature branch workflow (git branch)
    * Pick up paper and pen: draw any relevant schema or list of what you are going to implement
    * Implement features:
        * Break into testable pieces, find useful community packages
        * TDD: Write test → test fails → code → test succeeds
    * Don't forget you need to create DB tables!

---
class:small-title

# Continuous Integration & Deployment

* CI: merge (VCS) developers work several times a day (automated build and test)
* CD: automate deployment to production
* Preliminary (automate locally):
    * Lint (check source)
    * Install dependencies (external modules)
    * Build (optimize source)
    * Test (run test suites against build)
    * Serve (start build)
* Launch these steps remotely on code delivery (commit push or branch merge for instance), and before serving:
    * Push code to production servers
    * Configure (securely retrieve environment variables, eg database connection parameters)
* Implementations available for gitlab, github, bitbucket...

---
background-image: url(assets/img/cicd.png)

# Automated Pipeline with Review

.attribution[[Source: gitlab](https://docs.gitlab.com/ee/ci/introduction/)]

---

# Gitlab CI Configuration

<table><tr><td><pre style="margin-right:20px"><code class="hljs yaml remark-code">image: node:latest

stages:
  - build
  - test
  - deploy

before_script:
  - npm install

cache:
  paths:
    - node_modules/

build:
  type: build
  script:
    - npm run build
  artifacts:
    paths:
    - build
    expire_in: 1 day
</code></pre></td><td><pre><code class="hljs remark-code">lint:
  type: test
  script:
    - npm run lint
test:
  type: test
  dependencies:
    - build
  script:
    - npm run test

deploy:
  type: deploy
  dependencies:
    - build
  script:
    - apt-get update -qy
    - apt-get install -y ruby ruby-dev rubygems-integration
    - gem install dpl
    - dpl --provider=heroku --app=$APP --api-key=$KEY --skip-cleanup
  only:
    - master
</code></pre></td></tr></table>

---

# Receipts Manager: Deploy to Heroku (3/3)

.tutorial[Tutorial CI/CD]:
* Create a Heroku account
* Create a Heroku app `myapp-unique-name`
* Add a free PostgreSQL Heroku add-on
* Set CI/CD environment variables in Gitlab user interface:
    * Heroku app
    * Heroku API key
    * Database URL
* Push a code update to your gitlab repo to trigger the pipeline
* Check the pipeline output on gitlab.com
* Go to `http://myapp-unique-name.herokuapp.com/now`

---
name: SPA
class: center, middle

# Front-End: SPA & Tools

---
class: bottom
background-image: url(assets/img/spa.png)

.attribution[Source:<br/>[MSDN Magazine](https://msdn.microsoft.com/en-us/magazine/dn463786.aspx)]

---

# Components of a Webapp

* User interface (render data, manage user interaction)
* Router (map client-side URLs with different UI)
* Client-side data management (update local state, impact UI)
* API communication (get/update local state from/to servers)

Tooling:
* ES6+ Compilation
* Bundling (process and bundle JS, CSS, images)
* Static type checking
* Automated front-end tests

---

# React Webapp Example

* User interface → **React** + **CSS framework**
* Router → **React Router**
* Client-side data management → **Redux**
* API communication → **window.fetch**

Tooling:
* ES6+ Compilation → **Babel**
* Bundling → **webpack**
* Static type checking → **PropTypes** or **Flow** or **TypeScript**
* Automated front-end tests → **jest** + **enzyme**

Possibly use **create-react-app** to have tooling set up for you.

React is a *library*!

---

# Angular Webapp Example

* User interface → **Angular** components + **CSS framework**
* Router → **Angular** router
* Client-side data management → **Angular** services
* API communication → **Angular** HttpClient

Tooling:
* ES6+ Compilation → **Angular** compiler
* Bundling → **Angular** CLI
* Static type checking → **TypeScript**
* Automated front-end tests → **Karma**

Angular is a *framework*!

---

# Progressive Web Applications

* PWA = a website one may "install" to the OS home screen
* Highlights:
    * One-step installation outside the store (not submission, relies on SEO@)
    * First access to content is faster
    * PWA are shared with URLs!
    * Baked with front-end technologies (for iOS and Android)
    * Let's check [what web can do today](https://whatwebcando.today/)
* But:
    * Code is not native (performance, unavailable API: SMS, NFC, inter-app communication...)
    * Not perfectly supported by all web browsers (iOS < Android)
    * PWA installation is always free

.fit[![pwa](assets/img/pwa.png)]
.attribution[[Source](https://www.c-sharpcorner.com/article/making-web-sites-look-like-native-apps-without-the-app-store/)]

---

# Developing a PWA

* Declare a manifest (theme options like icons, title, orientation)
* Responsive design
* Serve through HTTPS
* Use service workers (check [workbox](https://developers.google.com/web/tools/workbox/)):
    * At least to enable offline mode
    * Various remote  caching strategies

.fit.alt[![workbox](assets/img/workbox.png)]

.attribution[Stale-While-Revalidate stragey, [source](https://developers.google.com/web/tools/workbox/modules/workbox-strategies)]

---

# Webapp Analysis tools

* Developer Tools Audits tab and Lighthouse chrome extension
* Speed test: [WebPageTest](https://www.webpagetest.org), [pingdom](https://tools.pingdom.com/)
* SSL test: [SSL Labs](https://www.ssllabs.com/ssltest/)
* SEO: [woorank](https://www.woorank.com/fr/), [SEO Web Page Analyzer](http://www.seowebpageanalyzer.com/)
* Upime: [Uptime Robot](https://uptimerobot.com/), [pingdom](https://www.pingdom.com/)
* Monitoring tools depending on the stack

---
class: center, middle

# Thank you

.author[Guillaume Denis - 2019]

.cc[![CC](assets/img/cc.png)]
.cc[![CC](assets/img/by.png)]
.cc[![CC](assets/img/sa.png)]

.author.small[Slides [source](https://gitlab.com/silently/cc-webdev), created using [remark](https://github.com/gnab/remark)]
