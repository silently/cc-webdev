Web development crash course licensed under Creative Commons Attribution-ShareAlike 4.0 International.

To serve presentation locally, git clone and then proceed with:
```shell
$ npm install
$ npx http-server
```
